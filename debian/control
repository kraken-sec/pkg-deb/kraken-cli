Source: kraken-cli
Section: metapackages
Priority: optional
Maintainer: Kraken Team <kraken.team@warx-16.org>
Build-Depends: debhelper (>= 9)
Standards-Version: 4.2.1
Homepage: https://warx-16.org
Vcs-Git: https://gitlab.com/kraken-sec/pkg-deb/kraken-cli.git
Vcs-Browser: https://gitlab.com/kraken-sec/pkg-deb/kraken-cli


Package: kraken-admin-base
Architecture: all
Depends: ${misc:Depends},
 adduser,
 cryptsetup-initramfs,
 cryptsetup-run,
 ddpt,
 dfc,
 gdisk,
 gvfs,
 gvfs-backends,
 gvfs-fuse,
 inxi,
 iw,
 kexec-tools,
 haveged,
 libmtp-runtime,
 libmtp9,
 libparted-fs-resize0,
 libparted-i18n,
 libparted2,
 lnav,
 lshw,
 lsscsi,
 lvm2,
 mtp-tools,
 mt-st,
 multiarch-support,
 ncdu,
 neofetch,
 netcat-traditional,
 parted,
 pcmciautils,
 policykit-1,
 procinfo,
 rcconf,
 rfkill,
 samba,
 sg3-utils,
 snmpd,
 snmp,
 sudo,
 testdisk,
 usbutils,
#~ util-linux,		# Essential PKG
 util-linux-locales,
 zerofree,
Suggests: libmtp-doc,
 parted-doc,
 policykit-1-doc,
Description: Kraken OS Linux base system - administration part
 This is the base system of Kraken OS Linux.
 .
 This metapackage is a part of kraken-base, it depends on
 all the administrations packages needed by default
 on any Kraken OS system.


Package: kraken-apt-base
Architecture: all
Depends: ${misc:Depends},
 alien,
 apt-transport-https,
 apt-transport-tor,
 apt-listchanges,
 apt-utils,
 apt-src,
 apt-file,
 command-not-found,
 kali-debtags,
Suggests: apt-doc,
Description: Kraken OS Linux base system - apt part
 This is the base system of Kraken OS Linux.
 .
 This metapackage is a part of kraken-base, it depends on
 all tools that enhance package manager (APT), needed by default
 on any Kraken OS system.


Package: kraken-file-base
Architecture: all
Depends: ${misc:Depends},
 axel,
 aufs-tools,
 cifs-utils,
 git,
 lftp,
 libimobiledevice6,
 libimobiledevice-utils,
 mlocate,
 mc,
 p7zip-full,
 ranger,
 sshfs,
 tftp,
 udpcast,
 unison-all,
 unace,
 unrar | unar,
 unzip,
 upx-ucl,
Suggests: git-doc,
Description: Kraken OS Linux base system - file part
 This is the base system of Kraken OS Linux.
 .
 This metapackage is a part of kraken-base, it depends on
 all tools that enhance file browsing/managing, needed by default
 on any Kraken OS system.


Package: kraken-monitor-base
Architecture: all
Depends: ${misc:Depends},
 hddtemp,
 htop,
 iftop,
 iptraf-ng,
 lm-sensors,
 nethogs,
 tcpdump,
 wavemon,
Description: Kraken OS Linux base system - monitoring part
 This is the base system of Kraken OS Linux.
 .
 This metapackage is a part of kraken-base, it depends on
 all the monitoring tools needed by default on any Kraken OS system.


Package: kraken-security-base
Architecture: all
Depends: ${misc:Depends},
 apparmor-full,
 chkrootkit,
 clamav-full,
 rkhunter,
 openvpn,
 wireguard,
Description: Kraken OS Linux base system - securing part
 This is the base system of Kraken OS Linux.
 .
 This metapackage is a part of kraken-base, it depends on
 all the securing tools needed by default on any Kraken OS system.


Package: kraken-shell-base
Architecture: all
Depends: ${misc:Depends},
 asciinema,
 bash-completion,
 bc,
 info,
 ks-ohmyzsh | kraken-ohmyzsh,
 most,
 pinfo,
 screen,
 scrot,
 subversion,
 tmux,
 unifont,
 vim,
#~ vim-powerline,
 wgetpaste,
 whois,
 zsh,
Suggests: bash-doc,
 ks-ohmyzsh-doc | kraken-ohmyzsh-doc,
 vim-doc,
 zsh-doc,
Description: Kraken OS Linux base system - shell part
 This is the base system of Kraken OS Linux.
 .
 This metapackage is a part of kraken-base, it depends on
 all tools that enhance interactive shell, needed by default
 on any Kraken OS system.


Package: kraken-base
Architecture: all
Depends: ${misc:Depends},
#### sub metapack BEGIN
 kraken-admin-base,
 kraken-apt-base,
 kraken-file-base,
 kraken-monitor-base,
 kraken-security-base,
 kraken-shell-base,
#### sub metapack END
#~ kali-archive-keyring,
 kali-defaults,
 ks-archive-keyring | kraken-archive-keyring,
Suggests: kraken-doc-base,
 kraken-crypto-cli,
 kraken-multimedia-cli,
 kraken-web-cli,
Description: Kraken OS Linux base system
 This is the base system of Kraken OS Linux.
 .
 This metapackage depends on all the packages that are installed
 by default on any Kraken OS system.


Package: kraken-base-fr
Architecture: all
Depends: ${misc:Depends},
 kraken-base,
Suggests: kraken-doc-base-fr,
Description: Kraken OS Linux base system - French language
 This is the base system of Kraken OS Linux.
 .
 This metapackage depends on all the packages that are installed
 by default on any Kraken OS system, with French localisation (l10n).


Package: kraken-crypto-cli
Architecture: all
Depends: ${misc:Depends},
 keyutils,
 libcrypt-passwdmd5-perl,
 libcrypt-ssleay-perl,
 libdigest-sha-perl,
 libnet-ssleay-perl,
 libnss3-tools,
 zulucrypt-cli,
 zulumount-cli,
 zulusafe-cli,
Description: Kraken OS Linux cli selection - crypto
 This is a cli selection for Kraken OS Linux.
 .
 This metapackage provides cryptography utilities in command line.


Package: kraken-web-cli
Architecture: all
Depends: ${misc:Depends},
 links,
 lynx,
Description: Kraken OS Linux cli selection - web
 This is a cli selection for Kraken OS Linux.
 .
 This metapackage provides web utilities in command line.


Package: kraken-multimedia-cli
Architecture: all
Depends: ${misc:Depends},
 audiofile-tools,
 cdtool,
 ffmpeg,
 ffmpeg2theora,
 handbrake-cli,
 mediainfo,
 moc,
 vorbis-tools,
 wavpack,
Description: Kraken OS Linux cli selection - multimedia
 This is a cli selection for Kraken OS Linux.
 .
 This metapackage provides multimedia utilities in command line.


Package: kraken-doc-base
Architecture: all
Depends: ${misc:Depends},
 debian-faq,
 debian-handbook,
 debian-reference,
 doc-base,
 doc-debian,
 info2man,
 info2www,
 man2html,
 manpages,
 texinfo-doc-nonfree,
Description: Kraken OS Linux doc selection - base
 This is a documents selection for Kraken OS Linux.
 .
 This metapackage provides basic documentation.


Package: kraken-doc-base-fr
Architecture: all
Depends: ${misc:Depends},
 kraken-doc-base,
#-
 debian-faq-fr,
 debian-reference-fr,
 doc-debian-fr,
 doc-linux-fr-html,
 doc-linux-fr-pdf,
 manpages-fr,
Description: Kraken OS Linux doc selection - base - French language
 This is a documents selection for Kraken OS Linux.
 .
 This metapackage provides basic documentation, in French.


Package: kraken-doc-dev
Architecture: all
Depends: ${misc:Depends},
 debian-kernel-handbook,
 doc-rfc,
 manpages-dev,
Suggests: kraken-doc-base,
#-
Description: Kraken OS Linux doc selection - devel
 This is a documents selection for Kraken OS Linux.
 .
 This metapackage provides basic devel documentation.


Package: kraken-doc-dev-fr
Architecture: all
Depends: ${misc:Depends},
 kraken-doc-dev,
#-
 manpages-fr-dev,
 manpages-fr-extra,
Suggests: kraken-doc-base-fr,
#-
Description: Kraken OS Linux doc selection - devel - French language
 This is a documents selection for Kraken OS Linux.
 .
 This metapackage provides basic devel documentation, in French.


Package: apparmor-full
Architecture: all
Depends: ${misc:Depends},
 apparmor,
 apparmor-profiles,
 apparmor-profiles-extra,
 apparmor-utils,
Description: AppArmor full
 This metapackage depends on packages that enhance AppArmor
 on your Kraken system.


Package: clamav-full
Architecture: all
Depends: ${misc:Depends},
 clamav,
 clamav-daemon,
 clamav-freshclam,
 clamav-milter,
 clamav-testfiles,
 clamav-unofficial-sigs,
Suggests: clamav-docs,
Description: ClamAV full
 This metapackage depends on packages that enhance ClamAV
 on your Kraken system.


Package: selinux-full
Architecture: all
Depends: ${misc:Depends},
 selinux-policy-default,
 selinux-utils,
Suggests: selinux-policy-doc,
Description: SELinux full
 This metapackage depends on packages that enhance SELinux
 on your Kraken system.


Package: tasksel-full
Architecture: all
Depends: ${misc:Depends},
 tasksel,
 astro-tasks,
 blends-tasks,
 debichem-tasks,
 education-tasks,
 ezgo-tasks,
 games-tasks,
 gis-tasks,
 hamradio-tasks,
 junior-tasks,
 med-tasks,
 multimedia-tasks,
 science-tasks,
Description: All Debian tasks
 This metapackage provides all Debian tasks needed by tasksel.

