**kraken-cli**

*Source of Kraken OS debian metapackages - cli , release 2018.10.02*

# PACKAGING

* Install package :

```sh
sudo apt install build-essential devscripts debhelper fakeroot
```

* Build .deb :

```sh
git clone https://gitlab.com/kraken-sec/pkg-deb/kraken-cli.git
mkdir kraken-cli/kraken-cli-2018.10.02
cp -r kraken-cli/debian/ kraken-cli/kraken-cli-2018.10.02/
cd kraken-cli/kraken-cli-2018.10.02/
```

```sh
debuild -i -uc -us --build=all
```

